/**
* Copyright (c) 2020 Vuplex Inc. All rights reserved.
*
* Licensed under the Vuplex Commercial Software Library License, you may
* not use this file except in compliance with the License. You may obtain
* a copy of the License at
*
*     https://vuplex.com/commercial-library-license
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
#if VUPLEX_MRTK
    using Microsoft.MixedReality.Toolkit.Input;
#endif

namespace Vuplex.WebView {

    [HelpURL("https://developer.vuplex.com/webview/IPointerInputDetector")]
    public class CanvasPointerInputDetector : DefaultPointerInputDetector {

        RectTransform _cachedRectTransform;

        protected override Vector2 _convertToNormalizedPoint(PointerEventData pointerEventData) {

            var canvas = Utils.GetParentCanvas(transform);
            var camera = canvas == null || canvas.renderMode == RenderMode.ScreenSpaceOverlay ? null : canvas.worldCamera;
            Vector2 localPoint;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_getRectTransform(), pointerEventData.position, camera, out localPoint);
            return _convertToNormalizedPoint(localPoint);
        }

        protected override Vector2 _convertToNormalizedPoint(Vector3 worldPosition) {

            var localPoint = _getRectTransform().InverseTransformPoint(worldPosition);
            return _convertToNormalizedPoint(localPoint);
        }

        Vector2 _convertToNormalizedPoint(Vector2 localPoint) {

            var rectTransform = _getRectTransform();
            var width = rectTransform.rect.width;
            var height = rectTransform.rect.height;
            // localPoint's origin is in its center, so we need to translate it so
            // so that its origin is the upper left corner and also flip its y axis.
            var translatedPoint = new Vector2(
                width / 2 + localPoint.x,
                height / 2 - localPoint.y
            );
            var normalizedPoint = new Vector2(
                translatedPoint.x / width,
                translatedPoint.y / height
            );
            return normalizedPoint;
        }

        RectTransform _getRectTransform() {

            if (_cachedRectTransform == null) {
                _cachedRectTransform = GetComponent<RectTransform>();
            }
            return _cachedRectTransform;
        }

        protected override bool _positionIsZero(PointerEventData eventData) {

            return eventData.position == Vector2.zero;
        }

    // Code specific to Microsoft's Mixed Reality Toolkit.
    // To enable this code, add VUPLEX_MRTK to "Scripting Define Symbols" in Player Settings.
    #if VUPLEX_MRTK
        void Start() {
            // Add a NearInteractionTouchable script to allow touch interactions
            // to trigger the IMixedRealityPointerHandler methods.
            var touchable = gameObject.AddComponent<NearInteractionTouchableUnityUI>();
            touchable.EventsToReceive = TouchableEventType.Pointer;
        }
    #endif
    }
}
