/**
* Copyright (c) 2020 Vuplex Inc. All rights reserved.
*
* Licensed under the Vuplex Commercial Software Library License, you may
* not use this file except in compliance with the License. You may obtain
* a copy of the License at
*
*     https://vuplex.com/commercial-library-license
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
#if VUPLEX_MRTK
    using Microsoft.MixedReality.Toolkit.Input;
#endif

namespace Vuplex.WebView {

    [HelpURL("https://developer.vuplex.com/webview/IPointerInputDetector")]
    public class DefaultPointerInputDetector : MonoBehaviour,
                                               IPointerInputDetector,
                                               IBeginDragHandler,
                                               IDragHandler,
                                               IPointerDownHandler,
                                               IPointerEnterHandler,
                                               IPointerExitHandler,
                                               IPointerUpHandler,
                                            #if VUPLEX_MRTK
                                               IMixedRealityPointerHandler,
                                            #endif
                                               IScrollHandler {

        public event EventHandler<EventArgs<Vector2>> BeganDrag;

        public event EventHandler<EventArgs<Vector2>> Dragged;

        public event EventHandler<EventArgs<Vector2>> PointerDown;

        public event EventHandler PointerExited;

        public event EventHandler<EventArgs<Vector2>> PointerMoved;

        public event EventHandler<EventArgs<Vector2>> PointerUp;

        public event EventHandler<ScrolledEventArgs> Scrolled;

        public bool PointerMovedEnabled { get; set; }

        /// <see cref="IBeginDragHandler"/>
        public void OnBeginDrag(PointerEventData eventData) {

            var handler = BeganDrag;
            if (handler != null) {
                handler(this, _convertToEventArgs(eventData));
            }
        }

        /// <see cref="IDragHandler"/>
        public void OnDrag(PointerEventData eventData) {

            // The point is Vector3.zero when the user drags off of the screen.
            if (_positionIsZero(eventData)) {
                return;
            }
            var handler = Dragged;
            if (handler != null) {
                handler(this, _convertToEventArgs(eventData));
            }
        }

        /// <see cref="IPointerDownHandler"/>
        public virtual void OnPointerDown(PointerEventData eventData) {

            var handler = PointerDown;
            if (handler != null) {
                handler(this, _convertToEventArgs(eventData));
            }
        }

        /// <see cref="IPointerEnterHandler"/>
        public void OnPointerEnter(PointerEventData eventData) {

            _isHovering = true;
        }

        /// <see cref="IPointerExitHandler"/>
        public void OnPointerExit(PointerEventData eventData) {

            _isHovering = false;
            var handler = PointerExited;
            if (handler != null) {
                handler(this, EventArgs.Empty);
            }
        }

        /// <see cref="IPointerUpHandler"/>
        public virtual void OnPointerUp(PointerEventData eventData) {

            var handler = PointerUp;
            if (handler == null) {
                return;
            }
            handler(this, _convertToEventArgs(eventData));
        }

        /// <see cref="IScrollHandler"/>
        public void OnScroll(PointerEventData eventData) {

            var handler = Scrolled;
            if (handler != null) {
                var scrollDelta = new Vector2(
                    -eventData.scrollDelta.x,
                    -eventData.scrollDelta.y
                );
                var eventArgs = new ScrolledEventArgs(scrollDelta, _convertToNormalizedPoint(eventData));
                handler(this, eventArgs);
            }
        }

        bool _isHovering;

        EventArgs<Vector2> _convertToEventArgs(Vector3 worldPosition) {

            var screenPoint = _convertToNormalizedPoint(worldPosition);
            return new EventArgs<Vector2>(screenPoint);
        }

        EventArgs<Vector2> _convertToEventArgs(PointerEventData pointerEventData) {

            var screenPoint = _convertToNormalizedPoint(pointerEventData);
            return new EventArgs<Vector2>(screenPoint);
        }

        protected virtual Vector2 _convertToNormalizedPoint(PointerEventData pointerEventData) {

            return _convertToNormalizedPoint(pointerEventData.pointerCurrentRaycast.worldPosition);
        }

        protected virtual Vector2 _convertToNormalizedPoint(Vector3 worldPosition) {
            // Note: transform.parent is WebViewPrefabResizer
            var localPosition = transform.parent.InverseTransformPoint(worldPosition);
            return new Vector2(1 - localPosition.x, -1 * localPosition.y);
        }

        void _emitPointerMovedIfNeeded() {

            if (!(PointerMovedEnabled && _isHovering)) {
                return;
            }
            var handler = PointerMoved;
            if (handler != null) {
                var pointerEventData = _getLastPointerEventData();
                if (pointerEventData == null) {
                    return;
                }
                handler(this, _convertToEventArgs(pointerEventData));
            }
        }

        /// <summary>
        /// Unity's event system doesn't include a standard pointer event
        /// for hovering (i.e. there's no `IPointerHoverHandler` interface).
        /// So, this method implements the equivalent functionality by
        /// using the protected `PointerInputModule.GetLastPointerEventData()`
        /// method to detect where the pointer is hovering.
        /// </summary>
        PointerEventData _getLastPointerEventData() {

            var pointerInputModule = EventSystem.current.currentInputModule as PointerInputModule;
            if (pointerInputModule == null) {
                return null;
            }
            // Use reflection to get access to the protected `GetPointerData()`
            // method. Unity isn't going to change this API because most input modules
            // extend PointerInputModule. Note that `GetPointerData()` is used instead
            // of `GetLastPointerEventData()` because the latter doesn't work with
            // the Oculus SDK's OVRInputModule.
            var args = new object[] { PointerInputModule.kMouseLeftId, null, false };
            pointerInputModule.GetType().InvokeMember(
                "GetPointerData",
                BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic,
                null,
                pointerInputModule,
                args
            );
            // The second argument is an out param.
            var pointerEventData = args[1] as PointerEventData;
            return pointerEventData;
        }

        void Update() {

            _emitPointerMovedIfNeeded();
        }

        protected virtual bool _positionIsZero(PointerEventData eventData) {

            return eventData.pointerCurrentRaycast.worldPosition == Vector3.zero;
        }

    // Code specific to Microsoft's Mixed Reality Toolkit.
    // To enable this code, add VUPLEX_MRTK to "Scripting Define Symbols" in Player Settings.
    #if VUPLEX_MRTK

        bool _beganDragEmitted;

        /// <see cref="IMixedRealityPointerHandler"/>
        public void OnPointerClicked(MixedRealityPointerEventData eventData) {}

        /// <see cref="IMixedRealityPointerHandler"/>
        public void OnPointerDragged(MixedRealityPointerEventData eventData) {

            var handler = Dragged;
            if (!_beganDragEmitted) {
                _beganDragEmitted = true;
                handler = BeganDrag;
            }
            if (handler != null) {
                handler(this, _convertToEventArgs(eventData.Pointer.Result.Details.Point));
            }
        }

        /// <see cref="IMixedRealityPointerHandler"/>
        public void OnPointerDown(MixedRealityPointerEventData eventData) {

            // Set IsTargetPositionLockedOnFocusLock to false, or else the Point
            // coordinates will be locked and won't change in OnPointerDragged or OnPointerUp.
            eventData.Pointer.IsTargetPositionLockedOnFocusLock = false;
            _beganDragEmitted = false;
            var handler = PointerDown;
            if (handler != null) {
                handler(this, _convertToEventArgs(eventData.Pointer.Result.Details.Point));
            }
        }

        /// <see cref="IMixedRealityPointerHandler"/>
        public void OnPointerUp(MixedRealityPointerEventData eventData) {

            var handler = PointerUp;
            if (handler != null) {
                handler(this, _convertToEventArgs(eventData.Pointer.Result.Details.Point));
            }
        }

        void Start() {
            // Add a NearInteractionTouchable script to allow touch interactions
            // to trigger the IMixedRealityPointerHandler methods.
            var touchable = gameObject.AddComponent<NearInteractionTouchable>();
            touchable.EventsToReceive = TouchableEventType.Pointer;
            touchable.SetBounds(Vector2.one);
            touchable.SetLocalForward(new Vector3(0, 0, -1));
        }
    #endif
    }
}
