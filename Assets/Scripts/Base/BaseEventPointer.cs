﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BaseEventPointer : MonoBehaviour
{
    protected bool dragging;
    protected long lastPinchTimestamp = -1;
    protected Vector3 lastPinchedRaycast;

    PointerEventData InitializeEventData(Vector2 position, GameObject obj)
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = position;
        eventData.selectedObject = obj;

        return eventData;
    }

    protected Vector2 ConvertToScreenCoords(Camera camera, Vector3 position)
    {
        return camera.WorldToScreenPoint(position);
    }

    protected void Click(Vector2 position, GameObject obj, int clicks = 1)
    {
        var eventData = InitializeEventData(position, obj);
        eventData.clickCount = clicks;
        eventData.button = PointerEventData.InputButton.Left;

        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.pointerDownHandler);
        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.pointerUpHandler);
        Debug.Log("Clicked");
    }

    protected void BeginDrag(Vector2 position, GameObject obj)
    {
        var eventData = InitializeEventData(position, obj);
        eventData.button = PointerEventData.InputButton.Left;

        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.pointerDownHandler);
        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.beginDragHandler);
        Debug.Log("Began Dragging");
        
        dragging = true;
    }

    protected void EndDrag(Vector2 position, GameObject obj)
    {
        var eventData = InitializeEventData(position, obj);
        eventData.button = PointerEventData.InputButton.Left;

        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.endDragHandler);
        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.pointerUpHandler);
        Debug.Log("Stopped Dragging");

        lastPinchedRaycast = new Vector3();
        dragging = false;
    }

    protected void Drag(Vector2 position, GameObject obj)
    {
        var eventData = InitializeEventData(position, obj);
        eventData.button = PointerEventData.InputButton.Left;

        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.dragHandler);
    }

    protected void Hover(Vector2 position, GameObject obj)
    {
        var eventData = InitializeEventData(position, obj);

        ExecuteEvents.Execute(obj, eventData, ExecuteEvents.moveHandler);
    }
}
