﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuplex.WebView;

public class ButtonNavigation : MonoBehaviour
{
    [SerializeField]
    CanvasWebViewPrefab browserPrefab;
    public CanvasWebViewPrefab BrowserPrefab { get; set; }

    public string HomeURL = "https://www.google.com";
    public void goForward()
    {
        browserPrefab.WebView.GoForward();
    }

    public void goBack()
    {
        browserPrefab.WebView.GoBack();
    }

    public void goHome()
    {
        browserPrefab.WebView.LoadUrl(HomeURL);
    }

    public void changeDragMode()
    {
        if (browserPrefab.DragMode == DragMode.DragToScroll)
            browserPrefab.DragMode = DragMode.DragWithinPage;
        else
            browserPrefab.DragMode = DragMode.DragToScroll;
    }
}
